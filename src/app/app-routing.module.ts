import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'not-found',
    loadChildren: () => import('./components/page-not-found/page-not-found.module').then(m => m.PageNotFoundModule)
  },
  {
    path: '',
    loadChildren: () => import('./components/body/body.module').then(m => m.BodyModule)
  },
  {
    path: 'index',
    loadChildren: () => import('./components/body/body.module').then(m => m.BodyModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./components/body-admin/body-admin.module').then(m => m.BodyAdminModule)
  },
  // La ruta ** sirve para que cuando se desconoce la ruta solicitada cargue una por default
  { path: '**', redirectTo: 'not-found' }, // pathMatch: 'full'
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
