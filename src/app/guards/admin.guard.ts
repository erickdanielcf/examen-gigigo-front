import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginUserService } from '../services/login-user.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private _router: Router,
    private _loginUserService: LoginUserService
  ) { }

  canActivate(): boolean {
    const access_token = this._loginUserService.getToken();

    if (access_token) {
      return true;
    } else {
      this._router.navigate(['index/sign-in']);
      return false;
    }
  }
}
