import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from './global';
import { Token } from '../models/token';

@Injectable()
export class MemberService {
  private url: string;
  public identity: Token;
  public token: string;

  constructor(private http: HttpClient) {
    this.url = Global.url;
  }

  /**
   * Recupera el token del usuario logueado
   * @return String
   */
  getToken(): string {
    const token: string = localStorage.getItem('token');

    if (token !== undefined) {
      this.token = token;
    } else {
      this.token = null;
    }

    return this.token;
  }

  /**
   * get members
   * @return Observable
   */
  getMembers() {

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .get(this.url + 'member', options);
  }

  /**
   * Registro team
   * @param id
   * @return Observable
   */
  getTeam(id) {

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .get(`${this.url}team/${id}?showMembers=true`, options);
  }

  /**
   * eliminar member
   * @param id
   * @return Observable
   */
  deleteMember(id: string) {

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .delete(`${this.url}member/${id}`, options);
  }

  /**
   * editar Member
   * @param id
   * @param memberEdit
   * @return Observable
   */
  editMember(id: string, memberEdit: object) {
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .put(`${this.url}member/${id}`, memberEdit, options);
  }

  /**
   * Register member
   * @param  member
   * @return Observable
   */
  registerMember(member: object) {
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .post(this.url + 'member', member, options);
  }
}
