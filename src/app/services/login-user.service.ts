import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from './global';
import { Token } from '../models/token';
import { Login } from '../models/login';
import { RegisterUser } from '../models/register.user';

@Injectable()
export class LoginUserService {
  private url: string;
  public identity: Token;
  public token: string;

  constructor(private http: HttpClient) {
    this.url = Global.url;
  }

  /**
   * Recupera los datos del usuario logueado
   * @return Usuario
   */
  getIdentity(): Token {
    const identity: Token = JSON.parse(localStorage.getItem('identity'));

    if (identity !== undefined) {
      this.identity = identity;
    } else {
      this.identity = null;
    }

    return this.identity;
  }

  /**
   * Recupera el token del usuario logueado
   * @return String
   */
  getToken(): string {
    const token: string = localStorage.getItem('token');

    if (token !== undefined) {
      this.token = token;
    } else {
      this.token = null;
    }

    return this.token;
  }

  /**
   * Registro del usuario
   * @param  registrer
   * @return Observable
   */
  registerUsuario(registrer: RegisterUser) {
    const params = JSON.stringify(registrer);

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const options = { headers: myheaders };

    return this.http
      .post(this.url + 'auth/register', params, options);
  }

  /**
   * Metodo de login de usuario
   * @param login
   * @return Observable
   */
  loginUsuario(login: Login) {
    const params = JSON.stringify(login);

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const options = { headers: myheaders };

    return this.http
      .post(this.url + 'auth/login', params, options);
  }
}
