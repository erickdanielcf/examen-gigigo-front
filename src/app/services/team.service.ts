import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from './global';
import { Token } from '../models/token';

@Injectable()
export class TeamService {
  private url: string;
  public identity: Token;
  public token: string;

  constructor(private http: HttpClient) {
    this.url = Global.url;
  }

  /**
   * Recupera el token del usuario logueado
   * @return String
   */
  getToken(): string {
    const token: string = localStorage.getItem('token');

    if (token !== undefined) {
      this.token = token;
    } else {
      this.token = null;
    }

    return this.token;
  }

  /**
   * get teams
   * @return Observable
   */
  getTeams() {

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .get(this.url + 'team', options);
  }

  /**
   * Registro team
   * @param id
   * @return Observable
   */
  getTeam(id) {

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .get(`${this.url}team/${id}?showMembers=true`, options);
  }

  /**
   * eliminar team
   * @param id
   * @return Observable
   */
  deleteTeam(id: string) {

    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .delete(`${this.url}team/${id}`, options);
  }

  /**
   * editar team
   * @param id
   * @param teamEdit
   * @return Observable
   */
  editTeam(id: string, teamEdit: object) {
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .put(`${this.url}team/${id}`, teamEdit, options);
  }

  /**
   * Register team
   * @param team
   * @return Observable
   */
  registerTeam(team: object) {
    const myheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getToken()
    });

    const options = { headers: myheaders };

    return this.http
      .post(this.url + 'team', team, options);
  }
}
