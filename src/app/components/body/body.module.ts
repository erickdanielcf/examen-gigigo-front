import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BodyComponent } from './body.component';

import { BodyRoutingModule } from './body-routing.module';

@NgModule({
  declarations: [
    BodyComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    BodyRoutingModule
  ],
  exports: [
  ],
  providers: []
})
export class BodyModule { }
