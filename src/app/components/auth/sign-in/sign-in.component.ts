import { Component, OnInit } from '@angular/core';
import { Login } from '../../../models/login';
import { LoginUserService } from '../../../services/login-user.service';
import { FormControl } from '@angular/forms';
import { Token } from '../../../models/token';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  public login: Login;
  public email = new FormControl('');
  public password = new FormControl('');
  public token: Token;
  public exists_error: boolean = false;
  public errorMensaje: string;

  constructor(
    private _router: Router,
    private loginUserService: LoginUserService
  ) {
  }

  ngOnInit() {
  }

  /**
   * Obtiene el token y los datos del usuario al momento de realizar el login
   * @param  loginForm formulario de login
   */
  onSubmit(loginForm): void {
    this.login = new Login(this.email.value, this.password.value);

    // obtiene el token
    this.loginUserService.loginUsuario(this.login).subscribe(
      (data: any) => {
        localStorage.setItem('token', data.access_token);
        this.token = data;
        localStorage.setItem('identity', JSON.stringify(this.token));
        loginForm.reset();

        this._router.navigate(['/admin/team']);
      },
      (error: any) => {
        this.errorMensaje = error.error.message;
        this.exists_error = true;
      }
    );
  }

}
