import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Token } from '../../../models/token';
import { RegisterUser } from '../../../models/register.user';
import { Router } from '@angular/router';
import { LoginUserService } from '../../../services/login-user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registrer: RegisterUser;
  public name = new FormControl('');
  public email = new FormControl('');
  public password = new FormControl('');
  public repeatPassword = new FormControl('');
  public token: Token;
  public exists_error: boolean = false;
  public errorMensaje: string;

  constructor(
    private _router: Router,
    private loginUserService: LoginUserService
  ) { }

  ngOnInit() {
  }

  onSubmit(loginForm) {

    if (this.password.value !== this.repeatPassword.value) {
      this.exists_error = true;
      this.errorMensaje = 'Las contraseñas no coinciden';
      return;
    }

    this.registrer = new RegisterUser(this.name.value, this.email.value, this.password.value);
    this.loginUserService.registerUsuario(this.registrer).subscribe(
      (data: any) => {
        localStorage.setItem('token', data.access_token);
        this.token = data;
        localStorage.setItem('identity', JSON.stringify(this.token));
        loginForm.reset();

        this._router.navigate(['/admin/team']);
      },
      (error: any) => {
        this.errorMensaje = error.error.message;
        this.exists_error = true;
      }
    );
  }
}
