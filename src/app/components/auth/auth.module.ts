import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AuthComponent } from './auth.component';
import { RegisterComponent } from './register/register.component';
import { SignInComponent } from './sign-in/sign-in.component';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginUserService } from '../../services/login-user.service';

@NgModule({
  declarations: [
    AuthComponent,
    SignInComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    AuthRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
    // Descomentar en caso de requerir importar los componentes desde otro lado
    // AuthComponent,
    // SignInComponent,
    // RegisterComponent
  ],
  providers: [LoginUserService]
})
export class AuthModule { }
