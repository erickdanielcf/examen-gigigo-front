import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BodyAdminRoutingModule } from './body-admin-routing.module';
import { BodyAdminComponent } from './body-admin.component';
import { HeaderModule } from '../header/header.module';


@NgModule({
  declarations: [BodyAdminComponent],
  imports: [
    CommonModule,
    BodyAdminRoutingModule,
    HeaderModule
  ]
})
export class BodyAdminModule { }
