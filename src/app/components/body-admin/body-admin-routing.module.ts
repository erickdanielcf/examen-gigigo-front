import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodyAdminComponent } from './body-admin.component';


const routes: Routes = [{
  path: '',
  component: BodyAdminComponent,
  children: [
    {
      path: '',
      loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodyAdminRoutingModule { }
