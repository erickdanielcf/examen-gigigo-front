import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { MemberComponent } from './member/member.component';
import { TeamComponent } from './team/team.component';
import { AdminGuard } from '../../guards/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [ AdminGuard ],
    children: [
      {
        path: 'member',
        component: MemberComponent
      },
      {
        path: 'team',
        component: TeamComponent
      },
      { path: '**', redirectTo: 'team' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
