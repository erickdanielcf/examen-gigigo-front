import { Component, OnInit } from '@angular/core';
import { Member } from '../../../models/member';
import { MemberService } from '../../../services/member.service';
import { TeamService } from '../../../services/team.service';
import { Team } from '../../../models/team';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  public editId: string;
  public members: Member[];
  public teams: Team[];
  public nameEdit = new FormControl('');
  public emailEdit = new FormControl('');
  public teamSelect = new FormControl();
  public exists_error: boolean = false;
  public errorMensaje: string;
  public exists_errorSave: boolean = false;
  public errorMensajeSave: string;
  public newName = new FormControl('');
  public newEmail = new FormControl('');


  constructor(
    private memberService: MemberService,
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
    this.getMembers();
    this.getTeams();
  }

  getMembers() {
    this.memberService.getMembers().subscribe(
      (data: any) => {
        this.members = data.members.members;
      }
    );
  }

  getTeams() {
    this.teamService.getTeams().subscribe(
      (data: any) => {
        this.teams = data.teams.teams;
      }
    );
  }

  deleteMember(id) {
    this.memberService.deleteMember(id).subscribe(
      (data: any) => {
        this.getMembers();
      }
    );
  }

  editMember() {
    const editMember = {};

    if (this.nameEdit.value !== null && this.nameEdit.value !== '') {
      editMember['name'] = this.nameEdit.value;
    }

    if (this.emailEdit.value !== null && this.emailEdit.value !== '') {
      editMember['email'] = this.emailEdit.value;
    }

    if (this.teamSelect.value !== null && this.teamSelect.value !== '') {
      editMember['teamId'] = this.teamSelect.value;
    }

    this.memberService.editMember(this.editId, editMember).subscribe(
      (data: any) => {
        this.getMembers();
        this.errorMensaje = '';
        this.exists_error = false;
      },
      (error: any) => {
        this.errorMensaje = error.error.message;
        this.exists_error = true;
      }
    );
  }

  save() {
    const newMember = {
      name: this.newName.value,
      email: this.newEmail.value,
      teamId: this.teamSelect.value
    };

    this.memberService.registerMember(newMember).subscribe(
      (data: any) => {
        this.getMembers();
        this.newName = new FormControl('');
        this.newEmail = new FormControl('');
        this.errorMensajeSave =  '';
        this.exists_errorSave = false;
      },
      (error: any) => {
        this.errorMensajeSave =  error.error.message;
        this.exists_errorSave = true;
      }
    );
  }

  modal(id) {
    this.editId = id;
    this.nameEdit = new FormControl('');
    this.emailEdit = new FormControl('');
  }

}
