import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../../services/team.service';
import { Team } from '../../../models/team';
import { FormControl } from '@angular/forms';
import { Member } from '../../../models/member';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  public editId: string;
  public teams: Team[];
  public members: Member[];
  public nameEdit = new FormControl('');
  public newName = new FormControl('');
  public exists_error: boolean = false;
  public errorMensaje: string;
  public exists_errorSave: boolean = false;
  public errorMensajeSave: string;
  public exists_errorGet: boolean = false;
  public errorMensajeGet: string;

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
    this.getTeams();
  }

  save() {
    const newTeam = {
      name: this.newName.value
    };

    this.teamService.registerTeam(newTeam).subscribe(
      (data: any) => {
        this.getTeams();
        this.newName = new FormControl('');
        this.errorMensajeSave =  '';
        this.exists_errorSave = false;
      },
      (error: any) => {
        this.errorMensajeSave =  error.error.message;
        this.exists_errorSave = true;
      }
    );
  }

  deleteTeam(id) {
    this.teamService.deleteTeam(id).subscribe(
      (data: any) => {
        this.getTeams();
      }
    );
  }

  getTeams() {
    this.teamService.getTeams().subscribe(
      (data: any) => {
        this.teams = data.teams.teams;
      }
    );
  }

  editTeam() {
    const editTeamm = {
      name: this.nameEdit.value
    };

    this.teamService.editTeam(this.editId, editTeamm).subscribe(
      (data: any) => {
        this.getTeams();
        this.errorMensaje = '';
        this.exists_error = false;
      },
      (error: any) => {
        this.errorMensaje = error.error.message;
        this.exists_error = true;
      }
    );
  }

  viewMembers(id) {
    this.teamService.getTeam(id).subscribe(
      (data: any) => {
        this.members = data.team.members;
        this.errorMensajeGet = '';
        this.exists_errorGet = false;
      }, 
      (error: any) => {
        this.errorMensajeGet = error.error.message;
        this.exists_errorGet = true;
      }
    );
  }

  modal(id: string) {
    this.editId = id;
    this.nameEdit = new FormControl('');
  }

}
