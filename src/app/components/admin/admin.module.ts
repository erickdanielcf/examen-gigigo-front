import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { MemberComponent } from './member/member.component';
import { TeamComponent } from './team/team.component';
import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TeamService } from '../../services/team.service';
import { MemberService } from '../../services/member.service';
import { AdminGuard } from '../../guards/admin.guard';
import { LoginUserService } from '../../services/login-user.service';


@NgModule({
  declarations: [
    AdminComponent,
    MemberComponent,
    TeamComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [TeamService, MemberService, AdminGuard, LoginUserService]
})
export class AdminModule { }
