import { Member } from './member';
export class Team {

  constructor(
    public _id: string,
    public creationDate: Date,
    public members: Member | string[],
    public name: string
  ) { }

}
