export class Log {

	constructor(
		public method: string,
		public usuario: string,
		public ip: string,
		public fecha_registro: Date
	) { }

}
