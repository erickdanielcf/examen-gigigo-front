export class Token {

  constructor(
    public _id: string,
    public name: string,
    public email: string,
    public access_token: string
  ) { }

}
